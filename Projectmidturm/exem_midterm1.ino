#include <Servo.h> //servo library
Servo myservo; 

void setup()
 {
  myservo.attach(3);
  pinMode(A5,INPUT);
}

void loop()
{
  int potValue=analogRead(A5);
  potValue=map(potValue, 0, 1023, 0, 180);
  myservo.write(potValue);
}
  
